# FIT SDM model for European TREE species using BIOMOD2

Based on harmonized NFI data from Mauri et al. (2017), with climatic data extracted from Chelsa and CGIAR

* http://chelsa-climate.org/
* http://www.cgiar-csi.org/data/global-aridity-and-pet-database
* http://www.cgiar-csi.org/data/global-high-resolution-soil-water-balance

The analysis are done using the R package 'remake' see https://github.com/richfitz/remake to facilitate replicability.


# References

Mauri, Achille, Giovanni Strona, and Jesús San-Miguel-Ayanz 2017EU-Forest, a High-Resolution Tree Occurrence Dataset for Europe. Scientific Data 4: 160123.


